'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('maaveerars', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      full_name: {
        type: Sequelize.STRING,
      },
      operating_name: {
        type: Sequelize.STRING,
      },
      section: {
        type: Sequelize.STRING,
      },
      gender: {
        type: Sequelize.ENUM('Male', 'Female'),
      },
      address: {
        type: Sequelize.STRING,
      },
      date_of_birth: {
        type: Sequelize.DATE,
      },
      date_of_died: {
        type: Sequelize.DATE,
      },
      event: {
        type: Sequelize.STRING,
      },
      tired_house: {
        type: Sequelize.STRING,
      },
      further_detail: {
        type: Sequelize.TEXT('long'),
      },
      slug: {
        type: Sequelize.STRING,
      },
      image: {
        type: Sequelize.TEXT,
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: { model: 'users', key: 'id' },
        onDelete: 'CASCADE',
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      deleted_at: {
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Maaveerars');
  },
};
