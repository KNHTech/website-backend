'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'categories',
      [
        {
          title: 'அரசியல்',
          slug: 'அரசியல்-1',
          description: 'அரசியல்',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          title: 'கவிதை',
          slug: 'கவிதை-2',
          description: 'கவிதை',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          title: 'கட்டுரை',
          slug: 'கட்டுரை-3',
          description: 'கட்டுரை',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          title: 'இலக்கியம்',
          slug: 'இலக்கியம்-4',
          description: 'இலக்கியம்',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          title: 'சமயம்',
          slug: 'சமயம்-4',
          description: 'சமயம்',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          title: 'பண்பாடு',
          slug: 'பண்பாடு-5',
          description: 'பண்பாடு',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          title: 'தமிழீழ விம்பம்',
          slug: 'தமிழீழவிம்பம்-6',
          description: 'தமிழீழ விம்பம்          ',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          title: 'மறைக்கபட்ட  தமிழீழ படுகொலை',
          slug: 'மறைக்கபட்டதமிழீழபடுகொலை-7',
          description: 'மறைக்கபட்ட தமிழீழ  படுகொலை',
          created_at: new Date(),
          updated_at: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('categories', null, {});
  },
};
