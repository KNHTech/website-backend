const { Post ,Category, User} = require('./../models')
const BaseService = require('./base.service')
const Sequelize = require('sequelize')


const path = require('path');
const fs = require('fs');
const documentPath = 'storage/document';
const imagePath = 'storage/post';

const log4js = require('./../../config/log4js');
const log = log4js.getLogger('post.service.js');

/**
 * 
 * @param {*} page 
 * @param {*} itemPerPage 
 * @param {*} query 
 * @param {*} status 
 */
exports.getAll = (page, itemPerPage, query) => new Promise(async(resolve, reject) => {
    var condition = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
    let attributes = ['id', 'title', 'slug', 'content', 'image', 'is_active', 'created_at'];      
    let include = [
        {
            model: User,
            as: 'user',
            attributes: ['id', 'name']
        },
        {
            model: Category,
            as: 'category',
            attributes: ['id', 'title', 'slug', 'description']
        },
    ];
    let responce = await BaseService.paginate(Post, page, itemPerPage, log,  condition, attributes, include );
    resolve(responce)
  });


/**
 * 
 * @param {*} values 
 */
exports.create = (values, headers) =>new Promise(async(resolve, reject) => {
  let responce = await BaseService.create(Post, {...values,
    user_id: getToken(headers).key}
    , log);
  resolve(responce)
});

/**
 * 
 * @param {*} model 
 */
exports.show = (model) => new Promise(async(resolve, reject) => {
    let attributes = ['id', 'title', 'slug', 'content', 'document', 'image', 'is_active', 'created_at'];      
    let include = [
        {
            model: User,
            as: 'user',
            attributes: ['id', 'name']
        },
        {
            model: Category,
            as: 'category',
            attributes: ['id', 'title', 'slug', 'description']
        },
    ];   
    let responce = await BaseService.show(Post, model, log, attributes, include);
  resolve(responce)
});

/**
 *
 * @param {int} id
 * @param {Object} values
 * @returns{Object}
 */
exports.update = (id, values, headers) => new Promise(async(resolve, reject) => {
    if (!id) reject(new Error(`id can't be empty`));
    let responce = await BaseService.update(Post, id, {...values, user_id: getToken(headers).key}, log);
    resolve(responce)
});

/**
 *
 * @param {int} id
 * @returns {String}
 */
exports.delete = (id) => new Promise(async(resolve, reject) => {
    if (!id) reject(new Error(`id can't be empty`));
    let responce = await BaseService.delete(Post, id, log);
    resolve(responce)
});



/**
 * 
 * @param {*} query 
 * @param {*} except 
 * @param {*} status 
 */
exports.search = (query, except) => new Promise( async(resolve, reject) => {
    let queryFilter = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
    let selected = except ? { id: { [Sequelize.Op.not]: JSON.parse(except) } } : null;
    let condition = { [Sequelize.Op.and]: [queryFilter, selected] };

    let attributes = ['id', 'title', 'slug'];
    let values = await BaseService.search(Post, log, condition, attributes);
    resolve(values)
});

/**
 * 
 * @param {*} field 
 * @param {*} value 
 */
 exports.count = (field, value) => new Promise(async (resolve, reject) => {
  let values = await BaseService.count(Post, field, value, log);
  resolve(values)
})


/**
*
* @param field
* @param value
* @returns {Promise<unknown>}
*/
exports.find = (field, value) => new Promise(async (resolve, reject) => {
  let values = await BaseService.find(Post, field, value, log);
  resolve(values)
})


/**
 *
 * @param id
 * @param file
 * @returns {Promise<unknown>}
 */
 exports.documentUpload = (id, files) => new Promise(async (resolve, reject) => {
  if (!id) reject(new Error(`id can't be empty`));
  let file;
  let value;
  

  if (files == null) file = null;
  else file = files.documet;


  if (file != null) {
    let extension = path.extname(file.name);
    if (extension == '') extension = '.pdf';
    let fileName = `${id}${extension}`;
    let filePath = path.join(`${documentPath}/${fileName}`);

    if ((await fs.existsSync(documentPath)) === false) {
      await fs.mkdirSync(documentPath, { recursive: true }, (err) => {
        if (err) throw err;
      });
    }

    await file.mv(filePath);
    value = fileName;
  } else {
    value = null;
  }

  Post.update({ document: value }, { where: { id: id } })
    .then(() => {
      resolve('Document Successfully added.');
    })
    .catch((err) => {
      log.error(err);
      reject(err);
    });
  
  });

/**
 *
 * @param id
 * @param file
 * @returns {Promise<unknown>}
 */
 exports.imageUpload = (id, files) => new Promise(async (resolve, reject) => {
    if (!id) reject(new Error(`id can't be empty`));
    let file;
    let value;
    
  
    if (files == null) file = null;
    else file = files.image;
  
    if (file != null) {
      let extension = path.extname(file.name);
      if (extension == '') extension = '.jpg';
      let fileName = `${id}${extension}`;
      let filePath = path.join(`${imagePath}/${fileName}`);
  
      if ((await fs.existsSync(imagePath)) === false) {
        await fs.mkdirSync(imagePath, { recursive: true }, (err) => {
          if (err) throw err;
        });
      }
  
      await file.mv(filePath);
      value = fileName;
    } else {
      value = null;
    }
  
    Post.update({ image: value }, { where: { id: id } })
      .then(() => {
        resolve('Benner image Successfully added.');
      })
      .catch((err) => {
        log.error(err);
        reject(err);
      });
  
  });