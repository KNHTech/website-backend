const { Category } = require('./../models')
const BaseService = require('./base.service')
const Sequelize = require('sequelize')
const log4js = require('./../../config/log4js');
const log = log4js.getLogger('category.service.js');

/**
 * 
 * @param {*} page 
 * @param {*} itemPerPage 
 * @param {*} query 
 * @param {*} status 
 */
exports.paginate = (page, itemPerPage, query) => new Promise(async(resolve, reject) => {
    var condition = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
    let attributes = ['id', 'title', 'slug', 'description', 'is_active', 'created_at'];      
    let responce = await BaseService.paginate(Category, page, itemPerPage, log,  condition, attributes );
    resolve(responce)
  });

/**
 * 
 * @param {*} values 
 */
exports.create = (values) =>new Promise(async(resolve, reject) => {

  let responce = await BaseService.create(Category, values, log);
  resolve(responce)
});

/**
 * 
 * @param {*} model 
 */
exports.show = (model) => new Promise(async(resolve, reject) => {
    let attributes = ['id', 'title', 'slug', 'description', 'is_active', 'created_at'];      
  let responce = await BaseService.show(Category, model, log, attributes);
  resolve(responce)
});

/**
 *
 * @param {int} id
 * @param {Object} values
 * @returns{Object}
 */
exports.update = (id, values) => new Promise(async(resolve, reject) => {
    if (!id) reject(new Error(`id can't be empty`));
    let responce = await BaseService.update(Category, id, values, log);
    resolve(responce)
});

/**
 *
 * @param {int} id
 * @returns {String}
 */
exports.delete = (id) => new Promise(async(resolve, reject) => {
    if (!id) reject(new Error(`id can't be empty`));
    let responce = await BaseService.delete(Category, id, log);
    resolve(responce)
});



/**
 * 
 * @param {*} query 
 * @param {*} except 
 * @param {*} status 
 */
exports.search = (query, except) => new Promise( async(resolve, reject) => {
    let queryFilter = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
    let selected = except ? { id: { [Sequelize.Op.not]: JSON.parse(except) } } : null;
    let condition = { [Sequelize.Op.and]: [queryFilter, selected] };

    let attributes = ['id', 'title', 'slug'];
    let values = await BaseService.search(Category, log, condition, attributes);
    resolve(values)
});

/**
 * 
 * @param {*} field 
 * @param {*} value 
 */
 exports.count = (field, value) => new Promise(async (resolve, reject) => {
  let values = await BaseService.count(Category, field, value, log);
  resolve(values)
})


/**
*
* @param field
* @param value
* @returns {Promise<unknown>}
*/
exports.find = (field, value) => new Promise(async (resolve, reject) => {
  let values = await BaseService.find(Category, field, value, log);
  resolve(values)
})