const { Media ,User} = require('./../models')
const BaseService = require('./base.service')
const Sequelize = require('sequelize')
const log4js = require('./../../config/log4js');
const log = log4js.getLogger('Media.service.js');

/**
 * 
 * @param {*} page 
 * @param {*} itemPerPage 
 * @param {*} query 
 * @param {*} status 
 */
exports.paginate = (page, itemPerPage, query) => new Promise(async(resolve, reject) => {
    var condition = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
    let attributes = ['id', 'title', 'slug', 'url', 'type', 'is_active', 'created_at'];      
    let include = [
        {
            model: User,
            as: 'user',
            attributes: ['id', 'name']
        },
    ];
    let responce = await BaseService.paginate(Media, page, itemPerPage, log,  condition, attributes, include );
    resolve(responce)
  });

/**
 * 
 * @param {*} values 
 */
exports.create = (values, headers) =>new Promise(async(resolve, reject) => {
  let responce = await BaseService.create(Media, {
    ...values,
     user_id: getToken(headers).key
    }, log);
  resolve(responce)
});

/**
 * 
 * @param {*} model 
 */
exports.show = (model) => new Promise(async(resolve, reject) => {
    let attributes = ['id', 'title', 'slug', 'url', 'type', 'is_active', 'created_at']; 
    let include = [
        {
            model: User,
            as: 'user',
            attributes: ['id', 'name']
        },
        ];     
    let responce = await BaseService.show(Media, model, log, attributes, include);
  resolve(responce)
});

/**
 *
 * @param {int} id
 * @param {Object} values
 * @returns{Object}
 */
exports.update = (id, values, headers) => new Promise(async(resolve, reject) => {
    if (!id) reject(new Error(`id can't be empty`));
    let responce = await BaseService.update(Media, id, {...values, 
     user_id: getToken(headers).key
    }, log);
    resolve(responce)
});

/**
 *
 * @param {int} id
 * @returns {String}
 */
exports.delete = (id) => new Promise(async(resolve, reject) => {
    if (!id) reject(new Error(`id can't be empty`));
    let responce = await BaseService.delete(Media, id, log);
    resolve(responce)
});



/**
 * 
 * @param {*} query 
 * @param {*} except 
 * @param {*} status 
 */
exports.search = (query, except) => new Promise( async(resolve, reject) => {
    let queryFilter = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
    let selected = except ? { id: { [Sequelize.Op.not]: JSON.parse(except) } } : null;
    let condition = { [Sequelize.Op.and]: [queryFilter, selected] };

    let attributes = ['id', 'title', 'slug'];
    let values = await BaseService.search(Media, log, condition, attributes);
    resolve(values)
});

/**
 * 
 * @param {*} field 
 * @param {*} value 
 */
 exports.count = (field, value) => new Promise(async (resolve, reject) => {
  let values = await BaseService.count(Media, field, value, log);
  resolve(values)
})


/**
*
* @param field
* @param value
* @returns {Promise<unknown>}
*/
exports.find = (field, value) => new Promise(async (resolve, reject) => {
  let values = await BaseService.find(Media, field, value, log);
  resolve(values)
})