const { Post ,Category, User, Media, Maaveerar, Essay, Genocide} = require('./../models')
const BaseService = require('./base.service')
const Sequelize = require('sequelize')
const log4js = require('./../../config/log4js');
const log = log4js.getLogger('web.service.js');
const moment = require('moment');


/**
 * 
 * @param {*} page 
 * @param {*} itemPerPage 
 * @param {*} query 
 * @param {*} category 
 * @param {*} status 
 */
exports.getNews = (query, category, start, end) => new Promise(async(resolve, reject) => {
    var queryFilter = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
    let categoryFilter = category ? {category_id : category } : null;
    let startDate = start ? moment(new Date(start)).startOf('day').format('YYYY-MM-DD HH:mm:ss') : null;
    let EndDate = end ? moment(new Date(end)).endOf('day').format('YYYY-MM-DD HH:mm:ss') : null;

    var DateRangeFilter;

    if (startDate != null && EndDate != null) {
      DateRangeFilter = { created_at: { [Sequelize.Op.between]: [startDate, EndDate] } };
    }  else if(startDate != null && EndDate == null) {
      DateRangeFilter = { created_at: { [Sequelize.Op.between]: [ moment(startDate).startOf('day').format('YYYY-MM-DD HH:mm:ss'),  moment(startDate).endOf('day').format('YYYY-MM-DD HH:mm:ss')] } };
    } else {
      DateRangeFilter = null;
    }

    let condition = { [Sequelize.Op.and]: [queryFilter, categoryFilter, DateRangeFilter, {is_active: true}] };
    let attributes = ['id', 'title', 'slug', 'content', 'document', 'image', 'is_active', 'created_at'];      
    let include = [
        {
            model: User,
            as: 'user',
            attributes: ['id', 'name']
        },
        {
            model: Category,
            as: 'category',
            attributes: ['id', 'title', 'slug', 'description']
        },
    ];
    let responce = await BaseService.getAll(Post, 'DESC', log, attributes, condition, include );
    resolve(responce)
  });


/**
 * 
 * @param {*} query 
 * @param {*} category 
 * @returns 
 */
exports.getEssays = (query, category, start, end ) => new Promise(async(resolve, reject) => {
  var queryFilter = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
  let categoryFilter = category ? {category_id : category } : null;

  let startDate = start ? moment(new Date(start)).startOf('day').format('YYYY-MM-DD HH:mm:ss') : null;
  let EndDate = end ? moment(new Date(end)).endOf('day').format('YYYY-MM-DD HH:mm:ss') : null;

  var DateRangeFilter;

    if (startDate != null && EndDate != null) {
      DateRangeFilter = { created_at: { [Sequelize.Op.between]: [startDate, EndDate] } };
    }  else if(startDate != null && EndDate == null) {
      DateRangeFilter = { created_at: { [Sequelize.Op.between]: [ moment(startDate).startOf('day').format('YYYY-MM-DD HH:mm:ss'),  moment(startDate).endOf('day').format('YYYY-MM-DD HH:mm:ss')] } };
    } else {
      DateRangeFilter = null;
    }



  let condition = { [Sequelize.Op.and]: [queryFilter, categoryFilter, DateRangeFilter, {is_active: true}] };
  let attributes = ['id', 'title', 'slug', 'paragraph', 'image', 'is_active', 'created_at'];      
  let include = [
      {
          model: User,
          as: 'user',
          attributes: ['id', 'name']
      },
      {
          model: Category,
          as: 'category',
          attributes: ['id', 'title', 'slug', 'description']
      },
  ];
  let responce = await BaseService.getAll(Essay, 'DESC', log, attributes, condition, include );
  resolve(responce)
});

exports.getGenocides = (query, start, end) => new Promise(async(resolve, reject) => {
  var queryFilter = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
  let startDate = start ? moment(new Date(start)).startOf('day').format('YYYY-MM-DD HH:mm:ss') : null;
  let EndDate = end ? moment(new Date(end)).endOf('day').format('YYYY-MM-DD HH:mm:ss') : null;

  var DateRangeFilter;

    if (startDate != null && EndDate != null) {
      DateRangeFilter = { genocide_date: { [Sequelize.Op.between]: [startDate, EndDate] } };
    }  else if(startDate != null && EndDate == null) {
      DateRangeFilter = { genocide_date: { [Sequelize.Op.between]: [ moment(startDate).startOf('day').format('YYYY-MM-DD HH:mm:ss'),  moment(startDate).endOf('day').format('YYYY-MM-DD HH:mm:ss')] } };
    } else {
      DateRangeFilter = null;
    }

  let condition = { [Sequelize.Op.and]: [queryFilter, DateRangeFilter, {is_active: true}] };
  let attributes = ['id', 'title', 'slug', 'paragraph','genocide_date', 'image', 'is_active', 'created_at'];      
  let include = [
      {
          model: User,
          as: 'user',
          attributes: ['id', 'name']
      }
  ];
  let responce = await BaseService.getAll(Genocide, 'DESC', log, attributes, condition, include );
  resolve(responce)
});

  exports.getRadio = (query) => new Promise(async(resolve, reject) => {
    var queryFilter = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
    var condition = { [Sequelize.Op.and]: [{ type: 'Radio' }, queryFilter, {is_active: true}] };
    let attributes = ['id', 'title', 'slug', 'url', 'is_active', 'created_at'];   
    let include = [
      {
          model: User,
          as: 'user',
          attributes: ['id', 'name']
      },
      ];   
    let responce = await BaseService.getAll(Media, 'ASC', log,  attributes, condition, include );
    resolve(responce)
  });
  
  
  exports.getTelevision = (query) => new Promise(async(resolve, reject) => {
    var queryFilter = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
    var condition = { [Sequelize.Op.and]: [{ type: 'Television' }, queryFilter, {is_active: true}] };
    let attributes = ['id', 'title', 'slug', 'url', 'is_active', 'created_at'];   
    let include = [
      {
          model: User,
          as: 'user',
          attributes: ['id', 'name']
      },
      ];   
    let responce = await BaseService.getAll(Media, 'ASC', log,  attributes, condition, include );
    resolve(responce)
  });

exports.LoadMaaveerars = ( query, start, end ) =>
new Promise(async (resolve, reject) => {
  var queryFilter = query ? { full_name: { [Sequelize.Op.like]: `%${query}%` } } : null;
  let startDate = start ? moment(new Date(start)).startOf('day').format('YYYY-MM-DD HH:mm:ss') : null;
  let EndDate = end ? moment(new Date(end)).endOf('day').format('YYYY-MM-DD HH:mm:ss') : null;

  var DateRangeFilter;

    if (startDate != null && EndDate != null) {
      DateRangeFilter = { date_of_died: { [Sequelize.Op.between]: [startDate, EndDate] } };
    }  else if(startDate != null && EndDate == null) {
      DateRangeFilter = { date_of_died: { [Sequelize.Op.between]: [ moment(startDate).startOf('day').format('YYYY-MM-DD HH:mm:ss'),  moment(startDate).endOf('day').format('YYYY-MM-DD HH:mm:ss')] } };
    } else {
      DateRangeFilter = null;
    }


  let condition = { [Sequelize.Op.and]: [queryFilter, DateRangeFilter, {is_active: true}] };
  let attributes = ['id', 'full_name', 'operating_name',  'section',  'gender',  'address','date_of_birth','date_of_died',
    'event', 'tired_house','further_detail', 'slug', 'is_active', 'created_at', 'image' ];
    let include = [
      {
          model: User,
          as: 'user',
          attributes: ['id', 'name']
      },
  ];
  let responce = await BaseService.getAll( Maaveerar, 'ASC', log, attributes, condition, include );
  resolve(responce);
});

exports.getMaaveerar = ( slug) => new Promise(async (resolve, reject) => {
  let attributes = ['id', 'full_name', 'operating_name',  'section',  'gender',  'address','date_of_birth','date_of_died',
    'event', 'tired_house','further_detail', 'slug', 'is_active', 'created_at', 'image' ];
    let include = [
      {
          model: User,
          as: 'user',
          attributes: ['id', 'name']
      },
  ];
  Maaveerar.findOne({
    attributes: attributes,
    include: include,
    where: { slug: slug },
  })
    .then(resolve)
    .catch((err) => {
      log.error(err);
      reject(err);
    });

});


exports.getCategory = (query) => new Promise(async(resolve, reject) => {
    var queryFilter = query ? { title: { [Sequelize.Op.like]: `%${query}%` } } : null;
    let condition = { [Sequelize.Op.and]: [queryFilter, {is_active: true}] };
    let attributes = ['id', 'title', 'slug', 'description', 'is_active', 'created_at'];      
    let responce = await BaseService.getAll(Category, 'ASC', log,  attributes, condition );
    resolve(responce)
  });


  exports.getEssay = ( slug) => new Promise(async (resolve, reject) => {
     let attributes = ['id', 'title', 'slug', 'paragraph', 'image', 'is_active', 'created_at'];      
     let include = [
      {
          model: User,
          as: 'user',
          attributes: ['id', 'name']
      },
      {
          model: Category,
          as: 'category',
          attributes: ['id', 'title', 'slug', 'description']
      },
    ];
    Essay.findOne({
      attributes: attributes,
      include: include,
      where: { slug: slug },
    })
      .then(resolve)
      .catch((err) => {
        log.error(err);
        reject(err);
      });
  
  });

  
  exports.getGenocide = ( slug) => new Promise(async (resolve, reject) => {
    let attributes = ['id', 'title', 'slug', 'paragraph','genocide_date', 'image', 'is_active', 'created_at'];      
    let include = [
      {
          model: User,
          as: 'user',
          attributes: ['id', 'name']
      }
  ];
  Genocide.findOne({
     attributes: attributes,
     include: include,
     where: { slug: slug },
   })
     .then(resolve)
     .catch((err) => {
       log.error(err);
       reject(err);
     });
 
 });