const { User } = require('../../models')

const BaseService = require('../base.service')

const Sequelize = require('sequelize')
const bcrypt = require('bcryptjs');

const path = require('path');
const fs = require('fs');
const imagePath = 'storage/user';

const log4js = require('../../../config/log4js');
const log = log4js.getLogger('user.service.js');

/**
 * 
 * @param {*} page 
 * @param {*} itemPerPage 
 * @param {*} query 
 * @param {*} status 
 */
exports.getAll = (page, itemPerPage, query, status) => new Promise(async(resolve, reject) => {
    let statusData = status ? (status == 'active' ? true : false) : null;

    var queryFilter = query ? { name: { [Sequelize.Op.like]: `%${query}%` } } : null;
    var statusFilter = status ? { is_active: statusData } : null;
    var condition = { [Sequelize.Op.and]: [queryFilter, statusFilter] };

    let attributes = ['id', 'name', 'email', 'email_verified_at', 'is_active', 'created_at', 'image'];      
    let responce = await BaseService.paginate(User, page, itemPerPage, log,  condition, attributes );
    resolve(responce)


  });

/**
 * 
 * @param {*} values 
 */
exports.create = (values) =>new Promise(async(resolve, reject) => {
  values.password = bcrypt.hashSync(values.password, 8)
  let responce = await BaseService.create(User, values, log);
  resolve(responce)
});

/**
 * 
 * @param {*} model 
 */
exports.show = (model) => new Promise(async(resolve, reject) => {
  let attributes = ['id', 'name', 'email', 'email_verified_at', 'is_active', 'created_at', 'image'];
  let responce = await BaseService.show(User, model, log, attributes);
  resolve(responce)
});

/**
 *
 * @param {int} id
 * @param {Object} values
 * @returns{Object}
 */
exports.update = (id, values) => new Promise(async(resolve, reject) => {
    if (!id) reject(new Error(`id can't be empty`));
    values.password = bcrypt.hashSync(values.password, 8)
    let responce = await BaseService.update(User, id, values, log);
    resolve(responce)
});

/**
 *
 * @param {int} id
 * @returns {String}
 */
exports.delete = (id) => new Promise(async(resolve, reject) => {
    if (!id) reject(new Error(`id can't be empty`));
    let responce = await BaseService.delete(User, id, log);
    resolve(responce)
});



/**
 * 
 * @param {*} query 
 * @param {*} except 
 * @param {*} status 
 */
exports.search = (query, except, status) => new Promise( async(resolve, reject) => {
    let statusData = status ? (status == 'active' ? true : null) : null;

    let queryFilter = query ? { name: { [Sequelize.Op.like]: `%${query}%` } } : null;
    let selected = except ? { id: { [Sequelize.Op.not]: JSON.parse(except) } } : null;
    let statusFilter = status ? { is_active: statusData } : null;
    let condition = { [Sequelize.Op.and]: [queryFilter, statusFilter, selected] };

    let attributes = ['id', 'name'];
    let values = await BaseService.search(User, log, condition, attributes);
    resolve(values)
});



/**
 *
 * @param id
 * @param file
 * @returns {Promise<unknown>}
 */
 exports.imageUpload = (id, files) => new Promise(async (resolve, reject) => {
  if (!id) reject(new Error(`id can't be empty`));
  let file;
  let value;

  if (files == null) file = null;
  else file = files.image;

  if (file != null) {
    let extension = path.extname(file.name);
    if (extension == '') extension = '.jpg';
    let fileName = `${id}${extension}`;
    let filePath = path.join(`${imagePath}/${fileName}`);

    if ((await fs.existsSync(imagePath)) === false) {
      await fs.mkdirSync(imagePath, { recursive: true }, (err) => {
        if (err) throw err;
      });
    }

    await file.mv(filePath);
    value = fileName;
  } else {
    value = null;
  }

  User.update({ image: value }, { where: { id: id } })
    .then(() => {
      resolve('User Image Successfully added.');
    })
    .catch((err) => {
      log.error(err);
      reject(err);
    });

});


/**
 * 
 * @param {*} field 
 * @param {*} value 
 */
 exports.count = (field, value) => new Promise(async (resolve, reject) => {
  let values = await BaseService.count(User, field, value, log);
  resolve(values)
})


/**
*
* @param field
* @param value
* @returns {Promise<unknown>}
*/
exports.find = (field, value) => new Promise(async (resolve, reject) => {
  let values = await BaseService.find(User, field, value, log);
  resolve(values)
})