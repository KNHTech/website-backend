const { Category, Maaveerar, Media, Post, User } = require('./../models');
const Sequelize = require('sequelize');

const log4js = require('./../../config/log4js');
const log = log4js.getLogger('dashboard.service.js');

/**
 *
 * @param {*} page
 * @param {*} itemPerPage
 * @param {*} query
 * @param {*} status
 */
exports.summery = () =>
  new Promise(async (resolve, reject) => {
    let response = {
      total_category: await Category.count(),
      active_category: await Category.count({ where: { is_active: true } }),

      total_maaveerar: await Maaveerar.count(),
      active_maaveerar: await Maaveerar.count({ where: { is_active: true } }),

      total_post: await Post.count(),
      active_post: await Post.count({ where: { is_active: true } }),

      total_user: await User.count(),
      active_user: await User.count({ where: { is_active: true } }),

      total_media: await Media.count(),
      active_media: await Media.count({ where: { is_active: true } }),

      total_media_radio: await Media.count({ where: { type: 'Radio' } }),
      active_media_radio: await Media.count({
        where: { [Sequelize.Op.and]: [{ type: 'Radio' }, { is_active: true }] },
      }),

      total_media_television: await Media.count({
        where: { type: 'Television' },
      }),
      active_media_television: await Media.count({
        where: {
          [Sequelize.Op.and]: [{ type: 'Television' }, { is_active: true }],
        },
      }),
    };
    resolve(response);
  });
