const { Maaveerar, User } = require('../models');
const BaseService = require('./base.service');
const Sequelize = require('sequelize');

const path = require('path');
const fs = require('fs');
const imagePath = 'storage/maaveerar';

const log4js = require('../../config/log4js');
const log = log4js.getLogger('Maaveerar.service.js');

/**
 *
 * @param {*} page
 * @param {*} itemPerPage
 * @param {*} query
 * @param {*} status
 */
exports.getAll = (page, itemPerPage, query) =>
  new Promise(async (resolve, reject) => {
    var condition = query ? { full_name: { [Sequelize.Op.like]: `%${query}%` } } : null;
    let attributes = ['id', 'full_name', 'operating_name',  'section',  'gender',  'address','date_of_birth','date_of_died',
      'event', 'tired_house','further_detail', 'slug', 'is_active', 'created_at', 'image' ];
      let include = [
        {
            model: User,
            as: 'user',
            attributes: ['id', 'name']
        },
    ];
    let responce = await BaseService.paginate( Maaveerar, page,  itemPerPage,  log, condition, attributes, include );
    resolve(responce);
  });

/**
 *
 * @param {*} values
 */
exports.create = (values, headers) =>
  new Promise(async (resolve, reject) => {
    let responce = await BaseService.create(Maaveerar,  {...values,
       user_id: getToken(headers).key
      }, log);
    resolve(responce);
  });

/**
 *
 * @param {*} model
 */
exports.show = (model) =>
  new Promise(async (resolve, reject) => {
    let attributes = ['id', 'full_name', 'operating_name',  'section',  'gender',  'address','date_of_birth','date_of_died',
    'event', 'tired_house','further_detail', 'slug', 'is_active', 'created_at', 'image' ];
    let include = [
      {
          model: User,
          as: 'user',
          attributes: ['id', 'name']
      },
  ];
    let responce = await BaseService.show(Maaveerar, model, log, attributes, include);
    resolve(responce);
  });

/**
 *
 * @param {int} id
 * @param {Object} values
 * @returns{Object}
 */
exports.update = (id, values) =>
  new Promise(async (resolve, reject) => {
    if (!id) reject(new Error(`id can't be empty`));
    let responce = await BaseService.update(Maaveerar, id, values, log);
    resolve(responce);
  });

/**
 *
 * @param {int} id
 * @returns {String}
 */
exports.delete = (id) =>
  new Promise(async (resolve, reject) => {
    if (!id) reject(new Error(`id can't be empty`));
    let responce = await BaseService.delete(Maaveerar, id, log);
    resolve(responce);
  });


/**
 * 
 * @param {*} field 
 * @param {*} value 
 */
 exports.count = (field, value) => new Promise(async (resolve, reject) => {
  let values = await BaseService.count(Maaveerar, field, value, log);
  resolve(values)
})


/**
*
* @param field
* @param value
* @returns {Promise<unknown>}
*/
exports.find = (field, value) => new Promise(async (resolve, reject) => {
  let values = await BaseService.find(Maaveerar, field, value, log);
  resolve(values)
})



/**
 *
 * @param id
 * @param file
 * @returns {Promise<unknown>}
 */
 exports.imageUpload = (id, files) => new Promise(async (resolve, reject) => {
  if (!id) reject(new Error(`id can't be empty`));
  let file;
  let value;

  if (files == null) file = null;
  else file = files.image;

  if (file != null) {
    let extension = path.extname(file.name);
    if (extension == '') extension = '.jpg';
    let fileName = `${id}${extension}`;
    let filePath = path.join(`${imagePath}/${fileName}`);

    if ((await fs.existsSync(imagePath)) === false) {
      await fs.mkdirSync(imagePath, { recursive: true }, (err) => {
        if (err) throw err;
      });
    }

    await file.mv(filePath);
    value = fileName;
  } else {
    value = null;
  }

  Maaveerar.update({ image: value }, { where: { id: id } })
    .then(() => {
      resolve('Maaveerar Image Successfully added.');
    })
    .catch((err) => {
      log.error(err);
      reject(err);
    });

});