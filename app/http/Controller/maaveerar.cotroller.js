const service = require('../../service/maaveerar.service');

const log4js = require('./../../../config/log4js');
const log = log4js.getLogger('maaveerar.controller.js');

/**
 * Display a listing of the resource
 *
 * @param {*} req
 * @param {Object} res
 */
exports.getAll = async (req, res) => {
  const { page, size, query } = req.query;
  service
    .getAll(page, size, query)
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};

/**
 *
 * @param {*} req
 * @param {*} res
 */
exports.create = async (req, res) => {
  const countByFullName = await service.count( 'full_name', req.body.full_name)
        .then((count) => { return count; })
        .catch(() => { return null; })

    if (countByFullName && countByFullName != 0) {
        res.status(500).send({ status: 500, error: {name: 'Full Name already exists.'}})
        return;
    }
  service
    .create(req.body,  req.headers)
    .then(async (doc) => response.successWithData(res, doc))
    .catch((err) => catchError(res, err, log));
};

/**
 *
 * @param {*} req
 * @param {*} res
 */
exports.show = async (req, res) =>
  service
    .show(req.params.id)
    .then((doc) => response.successWithData(res, doc))
    .catch((err) => catchError(res, err, log));

/**
 *
 * @param {*} req
 * @param {*} res
 */
exports.edit = async (req, res) => {
  const findByFullName = await service.find('full_name', req.body.full_name)
  .then((doc) => { return doc; })
  .catch(() => { return null; })

  if ((findByFullName != null ) && (findByFullName ? findByFullName.id  != parseInt(req.params.id) : null)) {
    res.status(500).send({ status: 500, error: {name: 'Full Name already exists.'}})
    return;
  }
  service
    .update(req.params.id, req.body)
    .then((doc) => response.successWithBoolean(res, doc))
    .catch((err) => catchError(res, err, log));
};
/**
 *  Remove the specified resource from storage.
 *
 * @param {id} req
 * @param {String} res
 */
exports.delete = async (req, res) =>
  service
    .delete(req.params.id)
    .then((doc) => response.successWithBoolean(res, doc))
    .catch((err) => catchError(res, err, log));

exports.imageUpload = async (req, res) => service.imageUpload(req.params.id, req.files)
    .then(data => response.successWithData(res, data))
    .catch(err => catchError(res, err, log));