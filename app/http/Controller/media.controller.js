const service = require('./../../service/media.service');

const log4js = require('./../../../config/log4js');
const log = log4js.getLogger('media.controller.js');

/**
 * Display a listing of the resource
 *
 * @param {*} req
 * @param {Object} res
 */
exports.paginate = async (req, res) => {
  const { page, size, query } = req.query;
  service
    .paginate(page, size, query)
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};

/**
 *
 * @param {*} req
 * @param {*} res
 */
exports.create = async (req, res) => {
  const countByTitle = await service
    .count('title', req.body.title)
    .then((count) => {
      return count;
    })
    .catch(() => {
      return null;
    });

  if (countByTitle && countByTitle != 0) {
    res
      .status(500)
      .send({ status: 500, error: { name: 'Title already exists.' } });
    return;
  }

  service
    .create(req.body, req.headers)
    .then(async (doc) => response.successWithData(res, doc))
    .catch((err) => catchError(res, err, log));
};

/**
 *
 * @param {*} req
 * @param {*} res
 */
exports.show = async (req, res) =>
  service
    .show(req.params.id)
    .then((doc) => response.successWithData(res, doc))
    .catch((err) => catchError(res, err, log));

/**
 *
 * @param {*} req
 * @param {*} res
 */
exports.edit = async (req, res) => {
  const findByTitle = await service
    .find('title', req.body.title)
    .then((doc) => {
      return doc;
    })
    .catch(() => {
      return null;
    });

  if (
    findByTitle != null &&
    (findByTitle ? findByTitle.id != parseInt(req.params.id) : null)
  ) {
    res
      .status(500)
      .send({ status: 500, error: { name: 'Title already exists.' } });
    return;
  }
  service
    .update(req.params.id, req.body, req.headers)
    .then((doc) => response.successWithBoolean(res, doc))
    .catch((err) => catchError(res, err, log));
};
/**
 *  Remove the specified resource from storage.
 *
 * @param {id} req
 * @param {String} res
 */
exports.delete = async (req, res) =>
  service
    .delete(req.params.id)
    .then((doc) => response.successWithBoolean(res, doc))
    .catch((err) => catchError(res, err, log));

/**
 *
 * @param {query} req
 * @param {object} res
 */
exports.search = async (req, res) => {
  service
    .search(req.query.query, req.query.except)
    .then((data) => response.successWithData(res, data))
    .catch((err) => catchError(res, err, log));
};
