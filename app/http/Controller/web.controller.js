const service = require('./../../service/web.service');

const log4js = require('./../../../config/log4js');
const log = log4js.getLogger('web.controller.js');

/**
 * Display a listing of the Radio
 *
 * @param {*} req
 * @param {Object} res
 */
exports.getNews = async (req, res) => {
  let { query, category, start, end } = req.query;
  service
    .getNews(query, category, start, end)
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};
/**
 * Display List Of essays
 * @param {*} req 
 * @param {*} res 
 */
exports.getEssays = async (req, res) => {
  let { query, category, start, end } = req.query;
  service
    .getEssays(query, category, start, end )
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};
/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.getGenocides = async (req, res) => {
  let { query, start, end } = req.query;
  service
    .getGenocides(query, start, end)
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};

/**
 * Display a listing of the Radio
 *
 * @param {*} req
 * @param {Object} res
 */
exports.getRadio = async (req, res) => {
  service
    .getRadio(req.query.query)
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};
/**
 * Display a listing of the Television
 *
 * @param {*} req
 * @param {Object} res
 */
exports.getTelevision = async (req, res) => {
  service
    .getTelevision(req.query.query)
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};
/**
 * Display a listing of the Maaveerar
 *
 * @param {*} req
 * @param {Object} res
 */
exports.LoadMaaveerars = async (req, res) => {
  let { query, start, end } = req.query;

  service
    .LoadMaaveerars(query, start, end )
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};
/**
 * Display a listing of the Maaveerar
 *
 * @param {*} req
 * @param {Object} res
 */
exports.getMaaveerar = async (req, res) => {
  service
    .getMaaveerar(req.params.slug)
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};

/**
 * Display a listing of the Maaveerar
 *
 * @param {*} req
 * @param {Object} res
 */
exports.getEssay = async (req, res) => {
  service
    .getEssay(req.params.slug)
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};

/**
 * Display a listing of the Maaveerar
 *
 * @param {*} req
 * @param {Object} res
 */
exports.getGenocide = async (req, res) => {
  service
    .getGenocide(req.params.slug)
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};

/**
 * Display a listing of the resource
 *
 * @param {*} req
 * @param {Object} res
 */
exports.getCategory = async (req, res) => {
  service
    .getCategory(req.query.query)
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
};
