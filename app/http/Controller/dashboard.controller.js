const service = require('./../../service/dashboard.service');

const log4js = require('./../../../config/log4js');
const log = log4js.getLogger('dashboard.controller.js');

/**
 * Display a listing of the resource
 *
 * @param {*} req
 * @param {Object} res
 */
exports.summery = async (req, res) =>
  service
    .summery()
    .then((doc) => res.send(doc))
    .catch((err) => catchError(res, err, log));
