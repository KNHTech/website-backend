'use strict';
const Joi = require('@hapi/joi');

const createRequest = Joi.object({
  title: Joi.string().required(),
  genocide_date: Joi.date().required(),
  paragraph: Joi.object().allow('', null),
  is_active: Joi.boolean().allow('', null),
}).options({ abortEarly: false });

const updateRequest = Joi.object({
  title: Joi.string().required(),
  genocide_date: Joi.date().required(),
  paragraph: Joi.object().allow('', null),
  is_active: Joi.boolean().allow('', null),
}).options({ abortEarly: false });

module.exports.createRequest = createRequest;
module.exports.updateRequest = updateRequest;
