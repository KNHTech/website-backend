'use strict';
const Joi = require('@hapi/joi');

const createRequest = Joi.object({
  title: Joi.string().required(),
  url: Joi.string().required(),
  type: Joi.string().valid('Television', 'Radio').required(),
  is_active: Joi.boolean().required(),
}).options({ abortEarly: false });

const updateRequest = Joi.object({
  title: Joi.string().required(),
  url: Joi.string().required(),
  type: Joi.string().valid('Television', 'Radio').required(),
  is_active: Joi.boolean().required(),
}).options({ abortEarly: false });

module.exports.createRequest = createRequest;
module.exports.updateRequest = updateRequest;
