'use strict';
const Joi = require('@hapi/joi');

const createRequest = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().required(),
  is_active: Joi.boolean().required(),
}).options({ abortEarly: false });

const updateRequest = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().required(),
  is_active: Joi.boolean().required(),
}).options({ abortEarly: false });

module.exports.createRequest = createRequest;
module.exports.updateRequest = updateRequest;
