'use strict';
const Joi = require('@hapi/joi');

const createRequest = Joi.object({
  full_name: Joi.string().required(),
  operating_name: Joi.string().required(),
  section: Joi.string().required(),
  gender: Joi.string().valid('Male', 'Female').required(),
  address: Joi.string().required(),
  date_of_birth: Joi.date().required(),
  date_of_died: Joi.date().required(),
  event: Joi.string().required(),
  tired_house: Joi.string().required(),
  further_detail: Joi.string().required(),
  is_active: Joi.boolean().required(),
}).options({ abortEarly: false });

const updateRequest = Joi.object({
  full_name: Joi.string().required(),
  operating_name: Joi.string().required(),
  section: Joi.string().required(),
  gender: Joi.string().valid('Male', 'Female').required(),
  address: Joi.string().required(),
  date_of_birth: Joi.date().required(),
  date_of_died: Joi.date().required(),
  event: Joi.string().required(),
  tired_house: Joi.string().required(),
  further_detail: Joi.string().required(),
  is_active: Joi.boolean().required(),

  is_active: Joi.boolean().required(),
}).options({ abortEarly: false });

module.exports.createRequest = createRequest;
module.exports.updateRequest = updateRequest;
