'use strict';
const imagePath = "user"
const moment = require('moment');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    email_verified_at: DataTypes.DATE,
    password: DataTypes.STRING,
    
    is_active: DataTypes.BOOLEAN,
    image: {
      type: DataTypes.STRING,
      get() {
          const img = this.getDataValue('image');
          if(img != null) {
              const image = `${env.appUrl}/${imagePath}/${img}`
              return image;
          }
      },
    },

  }, {
    sequelize,
    modelName: 'User',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  User.associate = function (models) {
    // associations can be defined here
  };
  
  return User;
};