'use strict';
const {trim,lowerCase} = require('lodash');
const moment = require('moment');
 
module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
    title: DataTypes.STRING,
    slug: DataTypes.STRING,    
    description: DataTypes.STRING,    
    is_active: DataTypes.BOOLEAN,
  }, {
    sequelize,
    modelName: 'Category',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  Category.associate = function (models) {
    // associations can be defined here
  };

  Category.addHook('afterCreate', async (doc) => {
    let slug =  `${lowerCase(doc.title).replace(/\s+/g, '')}-${doc.id}`;
    Category.update({ slug: slug }, { where: { id: doc.id } })
  });

  Category.addHook('afterUpdate', async (doc) => {
    let slug =  `${lowerCase(doc.title).replace(/\s+/g, '')}-${doc.id}`;
    Category.update({ slug: slug }, { where: { id: doc.id } })
  });
  
  return Category;
};