'use strict';
const {lowerCase} = require('lodash');
const moment = require('moment');


module.exports = (sequelize, DataTypes) => {
  const Media = sequelize.define(
    'Media',
    {
      title: DataTypes.STRING,
      slug: DataTypes.STRING,
      url: DataTypes.STRING,
      type: DataTypes.ENUM('Television', 'Radio'),
      user_id: DataTypes.INTEGER,
      is_active: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: 'Media',
      timestamps: true,
      underscored: true,
      paranoid: true,

      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );
  Media.associate = function (models) {
    Media.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user',
    });
  };

  Media.addHook('afterCreate', async (doc) => {
    let slug = `${lowerCase(doc.title).replace(/\s+/g, '')}-${doc.id}`;
    Media.update({ slug: slug }, { where: { id: doc.id } });
  });

  Media.addHook('afterUpdate', async (doc) => {
    let slug = `${lowerCase(doc.title).replace(/\s+/g, '')}-${doc.id}`;
    Media.update({ slug: slug }, { where: { id: doc.id } });
  });

  return Media;
};
