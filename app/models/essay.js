'use strict';
const {lowerCase} = require('lodash');
const moment = require('moment');

const imagePath = "essay";


module.exports = (sequelize, DataTypes) => {
  const Essay = sequelize.define('Essay', {
    title: DataTypes.STRING,
    slug: DataTypes.STRING,    
    paragraph: DataTypes.JSON,    
    category_id: DataTypes.INTEGER,    
    user_id: DataTypes.INTEGER,    
    is_active: DataTypes.BOOLEAN,
    image: {
      type: DataTypes.STRING,
      get() {
          const img = this.getDataValue('image');
          if(img != null) {
              const image = `${env.appUrl}/${imagePath}/${img}`
              return image;
          }
      },
    },
  }, {
    sequelize,
    modelName: 'Essay',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  Essay.associate = function (models) {
    Essay.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user',
    });
    Essay.belongsTo(models.Category, {
      foreignKey: 'category_id',
      onDelete: 'CASCADE',
      as: 'category',
    });
  };

  Essay.addHook('afterCreate', async (doc) => {
    let slug =  `${lowerCase(doc.title).replace(/\s+/g, '')}-${doc.id}`;
    Essay.update({ slug: slug }, { where: { id: doc.id } })
  });

  Essay.addHook('afterUpdate', async (doc) => {
    let slug =  `${lowerCase(doc.title).replace(/\s+/g, '')}-${doc.id}`;
    Essay.update({ slug: slug }, { where: { id: doc.id } })
  });
  
  return Essay;
};