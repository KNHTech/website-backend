'use strict';
const {lowerCase} = require('lodash');
const moment = require('moment');

const documentPath = "document";
const imagePath = "post";


module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    title: DataTypes.STRING,
    slug: DataTypes.STRING,    
    content: DataTypes.STRING,    
    category_id: DataTypes.INTEGER,    
    user_id: DataTypes.INTEGER,    
    is_active: DataTypes.BOOLEAN,
    document: {
      type: DataTypes.STRING,
      get() {
          const doc = this.getDataValue('document');
          if(doc != null) {
              const document = `${env.appUrl}/${documentPath}/${doc}`
              return document;
          }
      },
    },
    image: {
      type: DataTypes.STRING,
      get() {
          const img = this.getDataValue('image');
          if(img != null) {
              const image = `${env.appUrl}/${imagePath}/${img}`
              return image;
          }
      },
    },
  }, {
    sequelize,
    modelName: 'Post',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  Post.associate = function (models) {
    Post.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user',
    });
    Post.belongsTo(models.Category, {
      foreignKey: 'category_id',
      onDelete: 'CASCADE',
      as: 'category',
    });
  };

  Post.addHook('afterCreate', async (doc) => {
    let slug =  `${lowerCase(doc.title).replace(/\s+/g, '')}-${doc.id}`;
    Post.update({ slug: slug }, { where: { id: doc.id } })
  });

  Post.addHook('afterUpdate', async (doc) => {
    let slug =  `${lowerCase(doc.title).replace(/\s+/g, '')}-${doc.id}`;
    Post.update({ slug: slug }, { where: { id: doc.id } })
  });
  
  return Post;
};