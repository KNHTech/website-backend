'use strict';
const imagePath = "maaveerar";
const {lowerCase} = require('lodash')
const moment = require('moment');

module.exports = (sequelize, DataTypes) => {
  const Maaveerar = sequelize.define('Maaveerar', {
    full_name: DataTypes.STRING,
    operating_name: DataTypes.STRING,
    section: DataTypes.STRING,
    gender: DataTypes.ENUM('Male','Female'),
    address: DataTypes.STRING,
    date_of_birth: DataTypes.STRING,
    date_of_died: DataTypes.STRING,
    event: DataTypes.STRING,
    tired_house: DataTypes.STRING,
    further_detail: DataTypes.TEXT('long'),
    slug: DataTypes.STRING,
    is_active: DataTypes.BOOLEAN,
    image: {
      type: DataTypes.STRING,
      get() {
          const img = this.getDataValue('image');
          if(img != null) {
              const image = `${env.appUrl}/${imagePath}/${img}`
              return image;
          }
      },
    },
  //   created_at: {
  //     type: DataTypes.DATE,
  //     get() {
  //       const date = this.getDataValue('created_at');
  //       if(date != null) {
  //           const createdAt =  moment(new Date(date)).subtract(5, 'hours');
  //           return createdAt;
  //       }
  //   },
  // } 
  }, {
    sequelize,
    modelName: 'Maaveerar',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  Maaveerar.associate = function (models) {
    Maaveerar.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user',
    });
  };

  Maaveerar.addHook('afterCreate', async (doc) => {
    let slug =  `${lowerCase(doc.full_name).replace(/\s+/g, '')}-${doc.id}`;
    Maaveerar.update({ slug: slug }, { where: { id: doc.id } })
  });

  Maaveerar.addHook('afterUpdate', async (doc) => {
    let slug =  `${lowerCase(doc.full_name).replace(/\s+/g, '')}-${doc.id}`;
    Maaveerar.update({ slug: slug }, { where: { id: doc.id } })
  });
  
  return Maaveerar;
};