'use strict';
const {lowerCase} = require('lodash');
const moment = require('moment');

const imagePath = "genocide";


module.exports = (sequelize, DataTypes) => {
  const Genocide = sequelize.define('Genocide', {
    title: DataTypes.STRING,
    slug: DataTypes.STRING,    
    paragraph: DataTypes.JSON,    
    genocide_date: DataTypes.STRING,    
    user_id: DataTypes.INTEGER,    
    is_active: DataTypes.BOOLEAN,
    image: {
      type: DataTypes.STRING,
      get() {
          const img = this.getDataValue('image');
          if(img != null) {
              const image = `${env.appUrl}/${imagePath}/${img}`
              return image;
          }
      },
    },
  }, {
    sequelize,
    modelName: 'Genocide',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  Genocide.associate = function (models) {
    Genocide.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user',
    });
  };

  Genocide.addHook('afterCreate', async (doc) => {
    let slug =  `${lowerCase(doc.title).replace(/\s+/g, '')}-${doc.id}`;
    Genocide.update({ slug: slug }, { where: { id: doc.id } })
  });

  Genocide.addHook('afterUpdate', async (doc) => {
    let slug =  `${lowerCase(doc.title).replace(/\s+/g, '')}-${doc.id}`;
    Genocide.update({ slug: slug }, { where: { id: doc.id } })
  });
  
  return Genocide;
};