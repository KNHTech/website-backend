const express = require('express');
const router = express.Router();

/**      Auth route         */
router.use('/auth', require('./routers/auth/auth.router'));
/**      End auth route    */

/**      Settings route         */
router.use('/users', require('./routers/setting/user.router'));
/**      End settings route    */

router.use('/categories', require('./routers/category.router'));
router.use('/maaveerars', require('./routers/maaveerar.router'));
router.use('/media', require('./routers/media.router'));
router.use('/posts', require('./routers/post.router'));
router.use('/essays', require('./routers/essay.router'));
router.use('/genocides', require('./routers/genocide.router'));
router.use('/dashboard', require('./routers/dashboard.router'));

router.use('/web', require('./routers/web.router'));

module.exports = router;
