const express = require('express');
const router = express.Router();
const maaveerarController = require('./../../../../app/http/Controller/maaveerar.cotroller');

const {
  validateBodyWithToken,
  validateToken,
} = require('./../../../../util/validator.util');

const {
  createRequest,
  updateRequest,
} = require('./../../../../app/http/validator/maaveerar.validator');


router.post('/:id/image-upload',validateToken(), maaveerarController.imageUpload);

router
  .route('/')
  .get(validateToken(), maaveerarController.getAll)
  .post(validateBodyWithToken(createRequest), maaveerarController.create);

router
  .route('/:id')
  .get(validateToken(), maaveerarController.show)
  .patch(validateBodyWithToken(updateRequest), maaveerarController.edit)
  .delete(validateToken(), maaveerarController.delete);

module.exports = router;
