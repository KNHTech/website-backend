const express = require('express');
const router = express.Router();
const mediaController = require('./../../../../app/http/Controller/media.controller');

const {
  validateBodyWithToken,
  validateToken,
} = require('./../../../../util/validator.util');

const {
  createRequest,
  updateRequest,
} = require('./../../../../app/http/validator/media.validator');

router.get('/search', validateToken(), mediaController.search);

router
  .route('/')
  .get(validateToken(), mediaController.paginate)
  .post(validateBodyWithToken(createRequest), mediaController.create);

router
  .route('/:id')
  .get(validateToken(), mediaController.show)
  .patch(validateBodyWithToken(updateRequest), mediaController.edit)
  .delete(validateToken(), mediaController.delete);

module.exports = router;
