const express = require('express');
const router = express.Router();
const genocideController = require('./../../../../app/http/Controller/genocide.controller');

const {
  validateBodyWithToken,
  validateToken,
} = require('./../../../../util/validator.util');

const {
  createRequest,
  updateRequest,
} = require('./../../../../app/http/validator/genocide.validator');

router.post('/:id/image-upload',validateToken(), genocideController.imageUpload);

router
  .route('/')
  .get(validateToken(), genocideController.getAll)
  .post(validateBodyWithToken(createRequest), genocideController.create);

router
  .route('/:id')
  .get(validateToken(), genocideController.show)
  .patch(validateBodyWithToken(updateRequest), genocideController.edit)
  .delete(validateToken(), genocideController.delete);

module.exports = router;
