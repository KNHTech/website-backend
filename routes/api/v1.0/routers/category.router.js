const express = require('express');
const router = express.Router();
const categoryController = require('./../../../../app/http/Controller/category.controller');

const {
  validateBodyWithToken,
  validateToken,
} = require('./../../../../util/validator.util');

const {
  createRequest,
  updateRequest,
} = require('./../../../../app/http/validator/category.validator');

router.get('/search', validateToken(), categoryController.search);

router
  .route('/')
  .get(validateToken(), categoryController.paginate)
  .post(validateBodyWithToken(createRequest), categoryController.create);

router
  .route('/:id')
  .get(validateToken(), categoryController.show)
  .patch(validateBodyWithToken(updateRequest), categoryController.edit)
  .delete(validateToken(), categoryController.delete);

module.exports = router;
