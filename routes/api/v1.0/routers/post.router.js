const express = require('express');
const router = express.Router();
const postController = require('./../../../../app/http/Controller/post.controller');

const {
  validateBodyWithToken,
  validateToken,
} = require('./../../../../util/validator.util');

const {
  createRequest,
  updateRequest,
} = require('./../../../../app/http/validator/post.validator');


router.post('/:id/documet-upload',validateToken(), postController.documentUpload);
router.post('/:id/image-upload',validateToken(), postController.imageUpload);

router
  .route('/')
  .get(validateToken(), postController.getAll)
  .post(validateBodyWithToken(createRequest), postController.create);

router
  .route('/:id')
  .get(validateToken(), postController.show)
  .patch(validateBodyWithToken(updateRequest), postController.edit)
  .delete(validateToken(), postController.delete);

module.exports = router;
