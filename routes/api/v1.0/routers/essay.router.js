const express = require('express');
const router = express.Router();
const essayController = require('./../../../../app/http/Controller/essay.controller');

const {
  validateBodyWithToken,
  validateToken,
} = require('./../../../../util/validator.util');

const {
  createRequest,
  updateRequest,
} = require('./../../../../app/http/validator/essay.validator');

router.post('/:id/image-upload',validateToken(), essayController.imageUpload);

router
  .route('/')
  .get(validateToken(), essayController.getAll)
  .post(validateBodyWithToken(createRequest), essayController.create);

router
  .route('/:id')
  .get(validateToken(), essayController.show)
  .patch(validateBodyWithToken(updateRequest), essayController.edit)
  .delete(validateToken(), essayController.delete);

module.exports = router;
