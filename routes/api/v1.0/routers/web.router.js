const express = require('express');
const router = express.Router();
const webController = require('./../../../../app/http/Controller/web.controller');

router.get('/posts', webController.getNews);
router.get('/media/radio', webController.getRadio);
router.get('/media/television', webController.getTelevision);
router.get('/maaveerars', webController.LoadMaaveerars);
router.get('/maaveerars/:slug', webController.getMaaveerar);
router.get('/categories', webController.getCategory);
router.get('/essays', webController.getEssays);
router.get('/essays/:slug', webController.getEssay);
router.get('/genocides', webController.getGenocides);
router.get('/genocides/:slug', webController.getGenocide);


module.exports = router;
