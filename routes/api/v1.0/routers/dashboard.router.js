const express = require('express');
const router = express.Router();
const dashboardController = require('./../../../../app/http/Controller/dashboard.controller');

const { validateToken } = require('./../../../../util/validator.util');

router.get('/summery', validateToken(), dashboardController.summery);

module.exports = router;
