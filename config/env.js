require('dotenv/config');

const env = {
  appAddress: process.env.APP_ADDRESS,
  appPort: process.env.APP_PORT,
  appUrl: process.env.APP_URL ? process.env.APP_URL : `http://${process.env.APP_ADDRESS}:${process.env.APP_PORT}`,
};

module.exports = env;
